const { CustomApiError } = require("./error");
const { StatusCodes } = require("http-status-codes");

class UnauthenticatedError extends CustomApiError {
    constructor(message) {
        super(message);
        this.statusCode = StatusCodes.UNAUTHORIZED;
        this.status = false;
    }
}

module.exports = {
    UnauthenticatedError,
};
