const { CustomApiError } = require("./error");
const { StatusCodes } = require("http-status-codes");

class BadRequest extends CustomApiError {
    constructor(message) {
        super(message);
        this.statusCode = StatusCodes.BAD_REQUEST;
        this.status = false;
    }
}

module.exports = {
    BadRequest,
};
