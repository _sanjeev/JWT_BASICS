class CustomApiError extends Error {
    constructor(message) {
        super(message);
        // this.statusCode = statusCode;
        // this.status = status;
    }
}

const createCustomApiError = (message, statusCode, status) => {
    return new CustomApiError(message, statusCode, status);
};

module.exports = {
    createCustomApiError,
    CustomApiError,
};
