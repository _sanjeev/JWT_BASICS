const jwt = require("jsonwebtoken");
const { UnauthenticatedError, BadRequest, CustomApiError } = require("../error");

module.exports.authenticationMiddleWare = async (req, res, next) => {
    const authHeaders = req.headers.authorization;

    if (!authHeaders || !authHeaders.startsWith("Bearer ")) {
        throw new UnauthenticatedError("No token provided!");
    }
    const token = authHeaders.split(" ")[1];

    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const { id, userName } = decoded;
        req.user = { id, userName };
        next();
    } catch (error) {
        throw new UnauthenticatedError("Not authorize to access this route!");
    }
};
