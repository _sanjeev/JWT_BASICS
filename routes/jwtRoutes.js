const express = require("express");
const { dashboard, login } = require("../controllers/jwtController");
const { authenticationMiddleWare } = require("../middleware/auth");
const router = express.Router();

// router.get("/dashboard", dashboard);
router.route("/dashboard").get(authenticationMiddleWare, dashboard);
router.post("/login", login);

module.exports = router;
