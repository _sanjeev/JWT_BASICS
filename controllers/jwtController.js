const jwt = require("jsonwebtoken");
const { BadRequest } = require("../error");

module.exports.login = (req, res, next) => {
    const { userName, password } = req.body;
    if (!userName || !password) {
        throw new BadRequest("Please provide username and password");
    }
    const id = new Date().getDate();
    const token = jwt.sign(
        {
            id,
            userName,
        },
        process.env.JWT_SECRET,
        {
            expiresIn: "30d",
        }
    );
    return res.status(200).json({
        msg: "user created",
        token,
    });
};

module.exports.dashboard = async (req, res) => {
    const luckyNumber = Math.floor(Math.random() * 100);
    return res.status(200).json({
        status: true,
        message: "Success authenticated!",
        entity: {
            userName: req.user,
        },
    });
};
