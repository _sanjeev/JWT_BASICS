const express = require("express");
const dotenv = require("dotenv");
const { notFound } = require("./middleware/notFound");
const { errorHandler } = require("./middleware/errorHandler");
const connectDB = require("./db/connect");
require("express-async-errors");
dotenv.config();
const app = express();

const port = process.env.PORT || 5000;

app.use(express.json());
app.use("/", require("./routes"));
app.use(notFound);
app.use(errorHandler);

const start = async () => {
    try {
        await connectDB(process.env.MONGO_URI);
        app.listen(port, (err) => {
            if (err) {
                console.log("Error : ", err);
                return;
            }
            console.log(`Server is listening on port ${port}`);
        });
    } catch (error) {
        console.error("Error : ", error);
    }
};

start();
